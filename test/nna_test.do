vlib ./work

vlog -work work -sv hardware_scheduler.sv
vlog -work work bias_mem.v config_mem.v out_buf.v weight_mem.v dmem.v
vlog -work work -sv dw02_mac_sim.sv hypertan.sv neuron.sv relu.sv sigmoid.sv
vlog -work work nna.v 
vlog -work work -sv nna_tb.sv 

vsim -t 1pS -novopt nna_tb

view signals
view wave

do nna_wave.do

run -all