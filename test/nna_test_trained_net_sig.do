vlib ./work

vlog -work work -sv hardware_scheduler.sv
vlog -work work sig_bias_mem.v sig_config_mem.v sig_out_buf.v sig_weight_mem.v sig_dmem.v
vlog -work work -sv dw02_mac_sim.sv hypertan.sv neuron.sv relu.sv sigmoid.sv
vlog -work work nna.v 
vlog -work work -sv nna_tb_trained_net_sig.sv 

vsim -t 1pS -novopt nna_tb

view signals
view wave

do nna_wave_trained_net.do

run -all