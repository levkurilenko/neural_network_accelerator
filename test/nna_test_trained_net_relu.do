vlib ./work

vlog -work work -sv hardware_scheduler.sv
vlog -work work relu_bias_mem.v relu_config_mem.v relu_out_buf.v relu_weight_mem.v relu_dmem.v
vlog -work work -sv dw02_mac_sim.sv hypertan.sv neuron.sv relu.sv sigmoid.sv
vlog -work work nna.v 
vlog -work work -sv nna_tb_trained_net_relu.sv 

vsim -t 1pS -novopt nna_tb

view signals
view wave

do nna_wave_trained_net.do

run -all