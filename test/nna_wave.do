onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/rst
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/clk
add wave -noupdate -expand -group {NNA Testbench Top Level} {/nna_tb/wmem0/WMEM[10000]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal -childformat {{{/nna_tb/data_upper[15]} -radix hexadecimal} {{/nna_tb/data_upper[14]} -radix hexadecimal} {{/nna_tb/data_upper[13]} -radix hexadecimal} {{/nna_tb/data_upper[12]} -radix hexadecimal} {{/nna_tb/data_upper[11]} -radix hexadecimal} {{/nna_tb/data_upper[10]} -radix hexadecimal} {{/nna_tb/data_upper[9]} -radix hexadecimal} {{/nna_tb/data_upper[8]} -radix hexadecimal} {{/nna_tb/data_upper[7]} -radix hexadecimal} {{/nna_tb/data_upper[6]} -radix hexadecimal} {{/nna_tb/data_upper[5]} -radix hexadecimal} {{/nna_tb/data_upper[4]} -radix hexadecimal} {{/nna_tb/data_upper[3]} -radix hexadecimal} {{/nna_tb/data_upper[2]} -radix hexadecimal} {{/nna_tb/data_upper[1]} -radix hexadecimal} {{/nna_tb/data_upper[0]} -radix hexadecimal}} -subitemconfig {{/nna_tb/data_upper[15]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[14]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[13]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[12]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[11]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[10]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[9]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[8]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[7]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[6]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[5]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[4]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[3]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[2]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[1]} {-height 15 -radix hexadecimal} {/nna_tb/data_upper[0]} {-height 15 -radix hexadecimal}} /nna_tb/data_upper
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal /nna_tb/data_lower
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal /nna_tb/wmem_rd_data
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal /nna_tb/weight_mem_addr
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/weight_mem_wr_en
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal /nna_tb/omem_rd_data
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/dmem_offload_data_ctrl
add wave -noupdate -expand -group {NNA Testbench Top Level} -group BMEM -radix hexadecimal {/nna_tb/bmem0/BMEM[3]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group BMEM -radix hexadecimal {/nna_tb/bmem0/BMEM[2]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group BMEM -radix hexadecimal {/nna_tb/bmem0/BMEM[1]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group BMEM -radix hexadecimal {/nna_tb/bmem0/BMEM[0]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal -childformat {{{/nna_tb/cmem0/CMEM[7]} -radix hexadecimal} {{/nna_tb/cmem0/CMEM[6]} -radix hexadecimal} {{/nna_tb/cmem0/CMEM[5]} -radix hexadecimal} {{/nna_tb/cmem0/CMEM[4]} -radix hexadecimal} {{/nna_tb/cmem0/CMEM[3]} -radix hexadecimal} {{/nna_tb/cmem0/CMEM[2]} -radix hexadecimal} {{/nna_tb/cmem0/CMEM[1]} -radix hexadecimal} {{/nna_tb/cmem0/CMEM[0]} -radix hexadecimal}} -subitemconfig {{/nna_tb/cmem0/CMEM[7]} {-height 15 -radix hexadecimal} {/nna_tb/cmem0/CMEM[6]} {-height 15 -radix hexadecimal} {/nna_tb/cmem0/CMEM[5]} {-height 15 -radix hexadecimal} {/nna_tb/cmem0/CMEM[4]} {-height 15 -radix hexadecimal} {/nna_tb/cmem0/CMEM[3]} {-height 15 -radix hexadecimal} {/nna_tb/cmem0/CMEM[2]} {-height 15 -radix hexadecimal} {/nna_tb/cmem0/CMEM[1]} {-height 15 -radix hexadecimal} {/nna_tb/cmem0/CMEM[0]} {-height 15 -radix hexadecimal}} /nna_tb/cmem0/CMEM
add wave -noupdate -expand -group {NNA Testbench Top Level} -group WMEM -radix hexadecimal {/nna_tb/wmem0/WMEM[18]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group WMEM -radix hexadecimal {/nna_tb/wmem0/WMEM[17]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group WMEM -radix hexadecimal {/nna_tb/wmem0/WMEM[16]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group WMEM -radix hexadecimal {/nna_tb/wmem0/WMEM[15]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[31]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[30]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[29]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[28]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[27]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[26]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[25]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[24]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[23]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[22]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[21]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[20]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[19]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[18]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[17]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[16]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[15]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[14]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[13]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[12]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[11]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[10]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[9]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[8]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[7]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[6]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[5]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[4]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[3]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[2]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[1]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -group DMEM -radix hexadecimal {/nna_tb/dmem0/DMEM[0]}
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal -childformat {{{/nna_tb/omem0/OMEM[31]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[30]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[29]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[28]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[27]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[26]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[25]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[24]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[23]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[22]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[21]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[20]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[19]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[18]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[17]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[16]} -radix binary} {{/nna_tb/omem0/OMEM[15]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[14]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[13]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[12]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[11]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[10]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[9]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[8]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[7]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[6]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[5]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[4]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[3]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[2]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[1]} -radix hexadecimal} {{/nna_tb/omem0/OMEM[0]} -radix hexadecimal}} -expand -subitemconfig {{/nna_tb/omem0/OMEM[31]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[30]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[29]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[28]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[27]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[26]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[25]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[24]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[23]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[22]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[21]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[20]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[19]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[18]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[17]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[16]} {-height 15 -radix binary} {/nna_tb/omem0/OMEM[15]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[14]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[13]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[12]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[11]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[10]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[9]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[8]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[7]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[6]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[5]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[4]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[3]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[2]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[1]} {-height 15 -radix hexadecimal} {/nna_tb/omem0/OMEM[0]} {-height 15 -radix hexadecimal}} /nna_tb/omem0/OMEM
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/bmem_rd_data
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/bias_mem_addr
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/bias_mem_wr_en
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/config_mem_data
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/config_mem_addr
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/config_mem_wr_en
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/dmem_wr_mux_ctrl
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/dmem_mem_map_rd_data
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/dmem_wr_en_offload
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal /nna_tb/omem_wr_data
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/out_buf_mem_wr_en
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/out_buf_mem_addr
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal /nna_tb/alu_out
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal /nna_tb/result
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix decimal /nna_tb/nna_dmem_wr_addr
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/dmem_wr_en
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/dmem_rd_en
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/dmem_rd_data
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal /nna_tb/dmem_wr_data_final
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/dmem_rd_data_final
add wave -noupdate -expand -group {NNA Testbench Top Level} -radix hexadecimal /nna_tb/dmem_wr_addr_final
add wave -noupdate -expand -group {NNA Testbench Top Level} /nna_tb/dmem_wr_en_final
add wave -noupdate -expand -group Neurons -expand -group n0 -radix hexadecimal /nna_tb/nna0/n0/MAC
add wave -noupdate -expand -group Neurons -expand -group n0 -radix hexadecimal /nna_tb/nna0/n0/MAC_i
add wave -noupdate -expand -group Neurons -expand -group n0 /nna_tb/nna0/n0/activation
add wave -noupdate -expand -group Neurons -expand -group n0 -radix hexadecimal /nna_tb/nna0/n0/data
add wave -noupdate -expand -group Neurons -expand -group n0 /nna_tb/nna0/n0/data_width
add wave -noupdate -expand -group Neurons -expand -group n0 /nna_tb/nna0/n0/en
add wave -noupdate -expand -group Neurons -expand -group n0 /nna_tb/nna0/n0/output_width
add wave -noupdate -expand -group Neurons -expand -group n0 -radix hexadecimal /nna_tb/nna0/n0/result
add wave -noupdate -expand -group Neurons -expand -group n0 -radix hexadecimal /nna_tb/nna0/n0/result_hypertan
add wave -noupdate -expand -group Neurons -expand -group n0 -radix hexadecimal /nna_tb/nna0/n0/result_relu
add wave -noupdate -expand -group Neurons -expand -group n0 -radix hexadecimal /nna_tb/nna0/n0/result_sigmoid
add wave -noupdate -expand -group Neurons -expand -group n0 /nna_tb/nna0/n0/rst
add wave -noupdate -expand -group Neurons -expand -group n0 -radix hexadecimal /nna_tb/nna0/n0/weight
add wave -noupdate -expand -group Neurons -expand -group n0 -expand -group mac0 -radix hexadecimal /nna_tb/nna0/n0/MAC_inst/A
add wave -noupdate -expand -group Neurons -expand -group n0 -expand -group mac0 -radix hexadecimal /nna_tb/nna0/n0/MAC_inst/B
add wave -noupdate -expand -group Neurons -expand -group n0 -expand -group mac0 -radix hexadecimal /nna_tb/nna0/n0/MAC_inst/C
add wave -noupdate -expand -group Neurons -expand -group n0 -expand -group mac0 -radix hexadecimal /nna_tb/nna0/n0/MAC_inst/MAC
add wave -noupdate -expand -group Neurons -expand -group n1 /nna_tb/nna0/n1/MAC
add wave -noupdate -expand -group Neurons -expand -group n1 /nna_tb/nna0/n1/MAC_i
add wave -noupdate -expand -group Neurons -expand -group n1 /nna_tb/nna0/n1/activation
add wave -noupdate -expand -group Neurons -expand -group n1 -radix hexadecimal /nna_tb/nna0/n1/data
add wave -noupdate -expand -group Neurons -expand -group n1 /nna_tb/nna0/n1/data_width
add wave -noupdate -expand -group Neurons -expand -group n1 /nna_tb/nna0/n1/en
add wave -noupdate -expand -group Neurons -expand -group n1 /nna_tb/nna0/n1/output_width
add wave -noupdate -expand -group Neurons -expand -group n1 -radix hexadecimal /nna_tb/nna0/n1/result
add wave -noupdate -expand -group Neurons -expand -group n1 -radix hexadecimal /nna_tb/nna0/n1/result_hypertan
add wave -noupdate -expand -group Neurons -expand -group n1 -radix hexadecimal /nna_tb/nna0/n1/result_relu
add wave -noupdate -expand -group Neurons -expand -group n1 -radix hexadecimal /nna_tb/nna0/n1/result_sigmoid
add wave -noupdate -expand -group Neurons -expand -group n1 /nna_tb/nna0/n1/rst
add wave -noupdate -expand -group Neurons -expand -group n1 -radix hexadecimal /nna_tb/nna0/n1/weight
add wave -noupdate -expand -group Neurons -expand -group n1 -expand -group mac1 -radix hexadecimal /nna_tb/nna0/n1/MAC_inst/A
add wave -noupdate -expand -group Neurons -expand -group n1 -expand -group mac1 -radix hexadecimal /nna_tb/nna0/n1/MAC_inst/B
add wave -noupdate -expand -group Neurons -expand -group n1 -expand -group mac1 -radix hexadecimal /nna_tb/nna0/n1/MAC_inst/C
add wave -noupdate -expand -group Neurons -expand -group n1 -expand -group mac1 -radix hexadecimal -childformat {{{/nna_tb/nna0/n1/MAC_inst/MAC[15]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[14]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[13]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[12]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[11]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[10]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[9]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[8]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[7]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[6]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[5]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[4]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[3]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[2]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[1]} -radix hexadecimal} {{/nna_tb/nna0/n1/MAC_inst/MAC[0]} -radix hexadecimal}} -subitemconfig {{/nna_tb/nna0/n1/MAC_inst/MAC[15]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[14]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[13]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[12]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[11]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[10]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[9]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[8]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[7]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[6]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[5]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[4]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[3]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[2]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[1]} {-height 15 -radix hexadecimal} {/nna_tb/nna0/n1/MAC_inst/MAC[0]} {-height 15 -radix hexadecimal}} /nna_tb/nna0/n1/MAC_inst/MAC
add wave -noupdate -expand -group Neurons -expand -group n1 -expand -group n1_relu /nna_tb/nna0/n1/activation_a/operand
add wave -noupdate -expand -group Neurons -expand -group n1 -expand -group n1_relu /nna_tb/nna0/n1/activation_a/result
add wave -noupdate -expand -group Neurons -expand -group n2 /nna_tb/nna0/n2/MAC
add wave -noupdate -expand -group Neurons -expand -group n2 /nna_tb/nna0/n2/MAC_i
add wave -noupdate -expand -group Neurons -expand -group n2 /nna_tb/nna0/n2/activation
add wave -noupdate -expand -group Neurons -expand -group n2 -radix hexadecimal /nna_tb/nna0/n2/data
add wave -noupdate -expand -group Neurons -expand -group n2 /nna_tb/nna0/n2/data_width
add wave -noupdate -expand -group Neurons -expand -group n2 /nna_tb/nna0/n2/en
add wave -noupdate -expand -group Neurons -expand -group n2 /nna_tb/nna0/n2/output_width
add wave -noupdate -expand -group Neurons -expand -group n2 -radix hexadecimal /nna_tb/nna0/n2/result
add wave -noupdate -expand -group Neurons -expand -group n2 -radix hexadecimal /nna_tb/nna0/n2/result_hypertan
add wave -noupdate -expand -group Neurons -expand -group n2 -radix hexadecimal /nna_tb/nna0/n2/result_relu
add wave -noupdate -expand -group Neurons -expand -group n2 -radix hexadecimal /nna_tb/nna0/n2/result_sigmoid
add wave -noupdate -expand -group Neurons -expand -group n2 /nna_tb/nna0/n2/rst
add wave -noupdate -expand -group Neurons -expand -group n2 -radix hexadecimal /nna_tb/nna0/n2/weight
add wave -noupdate -expand -group Neurons -expand -group n2 -expand -group mac2 -radix hexadecimal /nna_tb/nna0/n2/MAC_inst/A
add wave -noupdate -expand -group Neurons -expand -group n2 -expand -group mac2 -radix hexadecimal /nna_tb/nna0/n2/MAC_inst/B
add wave -noupdate -expand -group Neurons -expand -group n2 -expand -group mac2 -radix hexadecimal /nna_tb/nna0/n2/MAC_inst/C
add wave -noupdate -expand -group Neurons -expand -group n2 -expand -group mac2 -radix hexadecimal /nna_tb/nna0/n2/MAC_inst/MAC
add wave -noupdate -expand -group Neurons -expand -group n3 /nna_tb/nna0/n3/MAC
add wave -noupdate -expand -group Neurons -expand -group n3 /nna_tb/nna0/n3/MAC_i
add wave -noupdate -expand -group Neurons -expand -group n3 /nna_tb/nna0/n3/activation
add wave -noupdate -expand -group Neurons -expand -group n3 -radix hexadecimal /nna_tb/nna0/n3/data
add wave -noupdate -expand -group Neurons -expand -group n3 /nna_tb/nna0/n3/data_width
add wave -noupdate -expand -group Neurons -expand -group n3 /nna_tb/nna0/n3/en
add wave -noupdate -expand -group Neurons -expand -group n3 /nna_tb/nna0/n3/output_width
add wave -noupdate -expand -group Neurons -expand -group n3 -radix hexadecimal /nna_tb/nna0/n3/result
add wave -noupdate -expand -group Neurons -expand -group n3 -radix hexadecimal /nna_tb/nna0/n3/result_hypertan
add wave -noupdate -expand -group Neurons -expand -group n3 -radix hexadecimal /nna_tb/nna0/n3/result_relu
add wave -noupdate -expand -group Neurons -expand -group n3 -radix hexadecimal /nna_tb/nna0/n3/result_sigmoid
add wave -noupdate -expand -group Neurons -expand -group n3 /nna_tb/nna0/n3/rst
add wave -noupdate -expand -group Neurons -expand -group n3 -radix hexadecimal -childformat {{{/nna_tb/nna0/n3/weight[7]} -radix octal} {{/nna_tb/nna0/n3/weight[6]} -radix octal} {{/nna_tb/nna0/n3/weight[5]} -radix octal} {{/nna_tb/nna0/n3/weight[4]} -radix octal} {{/nna_tb/nna0/n3/weight[3]} -radix octal} {{/nna_tb/nna0/n3/weight[2]} -radix octal} {{/nna_tb/nna0/n3/weight[1]} -radix octal} {{/nna_tb/nna0/n3/weight[0]} -radix octal}} -subitemconfig {{/nna_tb/nna0/n3/weight[7]} {-height 15 -radix octal} {/nna_tb/nna0/n3/weight[6]} {-height 15 -radix octal} {/nna_tb/nna0/n3/weight[5]} {-height 15 -radix octal} {/nna_tb/nna0/n3/weight[4]} {-height 15 -radix octal} {/nna_tb/nna0/n3/weight[3]} {-height 15 -radix octal} {/nna_tb/nna0/n3/weight[2]} {-height 15 -radix octal} {/nna_tb/nna0/n3/weight[1]} {-height 15 -radix octal} {/nna_tb/nna0/n3/weight[0]} {-height 15 -radix octal}} /nna_tb/nna0/n3/weight
add wave -noupdate -expand -group Neurons -expand -group n3 -expand -group mac3 -radix hexadecimal /nna_tb/nna0/n3/MAC_inst/A
add wave -noupdate -expand -group Neurons -expand -group n3 -expand -group mac3 -radix hexadecimal /nna_tb/nna0/n3/MAC_inst/B
add wave -noupdate -expand -group Neurons -expand -group n3 -expand -group mac3 -radix hexadecimal /nna_tb/nna0/n3/MAC_inst/C
add wave -noupdate -expand -group Neurons -expand -group n3 -expand -group mac3 -radix hexadecimal /nna_tb/nna0/n3/MAC_inst/MAC
add wave -noupdate -expand -group NNA /nna_tb/nna0/nn_finish
add wave -noupdate -expand -group NNA -radix hexadecimal /nna_tb/nna0/dmem_wr_addr
add wave -noupdate -expand -group NNA -radix hexadecimal /nna_tb/nna0/cmd
add wave -noupdate -expand -group NNA -radix hexadecimal /nna_tb/nna0/addr
add wave -noupdate -expand -group NNA -radix hexadecimal /nna_tb/nna0/dmem_wr_data
add wave -noupdate -expand -group NNA -radix hexadecimal /nna_tb/nna0/omem_wr_data
add wave -noupdate -expand -group NNA /nna_tb/nna0/write_data_ctrl
add wave -noupdate -expand -group {Hardware Scheduler} /nna_tb/nna0/hardware_scheduler0/cmd
add wave -noupdate -expand -group {Hardware Scheduler} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/addr
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Config Memory} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/config_mem_data
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Config Memory} /nna_tb/nna0/hardware_scheduler0/config_mem_wr_en
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Config Memory} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/config_mem_addr
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Bias Memory} /nna_tb/nna0/hardware_scheduler0/bias_mem_wr_en
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Bias Memory} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/bias_mem_addr
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Bias Memory} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/bias_mem_addr_ptr
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Output Buffer Memory} /nna_tb/nna0/hardware_scheduler0/out_buf_mem_wr_en
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Output Buffer Memory} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/out_buf_mem_addr_ptr
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Output Buffer Memory} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/out_buf_mem_addr_ptr_output
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Output Buffer Memory} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/out_buf_mem_addr_ptr_input
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Output Buffer Memory} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/out_buf_mem_addr
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Weight Memory} /nna_tb/nna0/hardware_scheduler0/weight_mem_wr_en
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Weight Memory} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/weight_mem_addr
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Weight Memory} -radix hexadecimal /nna_tb/nna0/hardware_scheduler0/weight_mem_addr_ptr
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Control Signals} /nna_tb/nna0/hardware_scheduler0/actv_ctrl
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Control Signals} /nna_tb/nna0/hardware_scheduler0/write_data_ctrl
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Control Signals} /nna_tb/nna0/hardware_scheduler0/out_ctrl
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Control Signals} /nna_tb/nna0/hardware_scheduler0/bias_ctrl
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group {Control Signals} /nna_tb/dmem_wr_mux_ctrl
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers /nna_tb/nna0/hardware_scheduler0/mac_en
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers /nna_tb/nna0/hardware_scheduler0/mac_rst
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers /nna_tb/nna0/hardware_scheduler0/nn_finish
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers /nna_tb/nna0/hardware_scheduler0/state
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers /nna_tb/nna0/hardware_scheduler0/op_code
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers /nna_tb/nna0/hardware_scheduler0/nn_begin
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers /nna_tb/nna0/hardware_scheduler0/next_op_code
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers -radix decimal /nna_tb/nna0/hardware_scheduler0/tot_node_cnt
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers -radix decimal /nna_tb/nna0/hardware_scheduler0/nna_dmem_wr_addr
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers -radix decimal /nna_tb/nna0/hardware_scheduler0/input_cnt
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers -radix decimal /nna_tb/nna0/hardware_scheduler0/num_layers
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers -radix decimal /nna_tb/nna0/hardware_scheduler0/current_layer
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers -radix decimal /nna_tb/nna0/hardware_scheduler0/nodes_in_layer
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers -radix decimal /nna_tb/nna0/hardware_scheduler0/pre_layer_nodes
add wave -noupdate -expand -group {Hardware Scheduler} -expand -group Registers /nna_tb/nna0/hardware_scheduler0/final_layer_flag
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {388 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 231
configure wave -valuecolwidth 259
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {883 ps}
