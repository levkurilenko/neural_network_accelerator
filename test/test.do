vlib ./work

vlog -work work -sv hardware_scheduler.sv
vlog -work work *.v

vsim -t 1pS -novopt hardware_scheduler_tb

view signals
view wave

do wave.do

run -all