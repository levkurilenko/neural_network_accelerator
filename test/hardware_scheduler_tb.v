// Name:    Lev Kurilenko
// Class:   EE 525
// Date:    2/28/2016
//
// Project: Neural Network Accelerator
// Module:  Hardware Scheduler Testbench
//

module hardware_scheduler_tb;

// ===== Reset & Clock =====
reg rst;
reg clk ;                                       // 200 MHz clock

// ===== Memory Mapped Dmem Signals =====
reg [15:0] cmd = 16'b1110_0_000001_0100_0;
reg [15:0] addr = 16'h0000;
reg [31:0] data = 32'h0000_0000;

// ===== Memory Outputs =====
wire [31:0] wmem_rd_data;
wire [31:0] omem_rd_data;
wire [31:0] bmem_rd_data;

// ===== Hardware Scheduler =====
wire [15:0] config_mem_data;                    // Configuration Memory Data

wire mac_en;
wire mac_rst;
wire [1:0] actv_ctrl;

wire bias_ctrl;                                 // Bias control for data input of Node
wire write_data_ctrl;                           // Mux control for Output Buffer Memory write data
wire [1:0] out_ctrl;                            // Output Buffer data control for 4-to-1 Mux

wire bias_mem_wr_en;                            // Bias Memory Write Enable
wire out_buf_mem_wr_en;                         // Output Buffer Memory Write Enable
wire weight_mem_wr_en;                          // Weight Memory Write Enable
wire config_mem_wr_en;                          // Configuration Memory Write Enable

wire [13:0] weight_mem_addr;                    // Weight Memory Address
wire [9:0]  bias_mem_addr;                      // Bias Memory Address
wire [4:0]  out_buf_mem_addr;                   // Output Buffer Memory Address
wire [2:0]  config_mem_addr;                    // Configuration Memory Addr

wire [15:0] dmem_wr_addr;
wire dmem_wr_mux_ctrl;


// ===== Clock and delay settings =====
parameter delay = 6;          // One entire period
parameter half_per = delay/2; // Half period
parameter ini = delay-0.3;    // setup time of 0.3

always begin
   #(half_per) clk <= ~clk;
end

// ===== Initialize test =====
initial begin
   // Assert reset
   rst = 1;
   clk = 0;
   // De-assert reset
   #(delay);
   rst = 0;
   #(delay + delay/2);
   cmd = 16'b1110_0_000010_0100_0;
   addr = 16'h0000;
   data = 32'hC0FF_EE00;
   #(delay);
   
   // ==== START NEURAL NETWORK ====
   cmd = 16'b1110_0_000010_0100_1;   // Begin Neural Network Accelerator
   addr = 16'h0000;
   data = 32'hC0FF_EE00;
   #(42*delay);

   // ==== Send to NOOP for 2 clock cycles ====
   cmd = 16'b1110_0_000010_0100_0;   // Begin Neural Network Accelerator
   addr = 16'h0000;
   data = 32'hC0FF_EE00;
   #(delay*2);
   
   // ==== Begin Output Data Offload ====
   cmd = 16'b1000_0_000010_0100_0;   // Begin Neural Network Accelerator
   addr = 16'h0f0c;
   data = 32'hC0FF_EE00;
   #(delay*30);
   
   $finish;
end
   

hardware_scheduler hardware_scheduler0(
    .rst(rst),
    .clk(clk),
    .cmd(cmd),
    .addr(addr),

    .config_mem_data(config_mem_data),

    .mac_en(mac_en),
    .mac_rst(mac_rst),
    .actv_ctrl(actv_ctrl),
    
    .bias_ctrl(bias_ctrl),
    .write_data_ctrl(write_data_ctrl),
    .out_ctrl(out_ctrl),

    .bias_mem_wr_en(bias_mem_wr_en),
    .out_buf_mem_wr_en(out_buf_mem_wr_en),
    .weight_mem_wr_en(weight_mem_wr_en),
    .config_mem_wr_en(config_mem_wr_en),

    .weight_mem_addr(weight_mem_addr),
    .bias_mem_addr(bias_mem_addr),
    .out_buf_mem_addr(out_buf_mem_addr),
    .config_mem_addr(config_mem_addr),
    
    .dmem_wr_addr(dmem_wr_addr),
    .dmem_wr_mux_ctrl(dmem_wr_mux_ctrl)
    
    // DATA MAY JUST BE ROUTED DIRECTLY INTO ALL OF THE MEMORIES WITH MUX AND WR_EN CONTROL
    // THIS SHOULD BE UPDATED IN DATAPATH TO REFLECT THE NEW DESIGN
    //input [32:0] data
);

wmem wmem0(
    .clk(clk),
    .rd_data(wmem_rd_data),
    .rd_en(weight_mem_wr_en),
    .wr_en(weight_mem_wr_en),
    .rd_addr(weight_mem_addr),
    .wr_addr(weight_mem_addr),
    .wr_data(data)
);

cmem cmem0(
    .clk(clk),
    .rd_data(config_mem_data),
    .rd_en(config_mem_wr_en),
    .wr_en(config_mem_wr_en),
    .rd_addr(config_mem_addr),
    .wr_addr(config_mem_addr),
    .wr_data(data[15:0])
);

omem omem0(
    .clk(clk),
    .rd_data(omem_rd_data),
    .rd_en(out_buf_mem_wr_en),
    .wr_en(out_buf_mem_wr_en),
    .rd_addr(out_buf_mem_addr),
    .wr_addr(out_buf_mem_addr),
    .wr_data(data)      // NEED TO CREATE INPUT MUX. FOR NOW JUST TESTING WRITING CAPABILITIES
);

bmem bmem0(
    .clk(clk),
    .rd_data(bmem_rd_data),
    .rd_en(bias_mem_wr_en),
    .wr_en(bias_mem_wr_en),
    .rd_addr(bias_mem_addr),
    .wr_addr(bias_mem_addr),
    .wr_data(data)
);

endmodule
