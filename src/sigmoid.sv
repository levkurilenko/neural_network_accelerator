module sigmoid #(
   parameter weight_width = 8,
   parameter data_width = 8,
   parameter output_width = 8
   ) (
   input [weight_width+data_width-1:0] operand,
   output reg [output_width-1:0] result
);

logic [output_width-1:0] result_temp;

always_comb begin
   if (operand[15] & ~(&operand[12:5])) begin  // if operand <= -64 (-2 in fixed point) set output to 0
      result = 8'b0_00_00000;
   end
   else if (~operand[15] & |operand[14:13]) begin  // >= 64 (2 in fixed point) set output to 1
      result = 8'b0_01_00000;
   end
   else begin
      result_temp = {{2{operand[12]}}, operand[12:7]};  // divide by 4
      // result = {result_temp[output_width-1:6], ~result_temp[5], result_temp[4:0]};  // add 32
      result = result_temp + 16; // add 16
   end
end

endmodule
