// Name:    Lev Kurilenko
// Class:   EE 525
// Date:    3/5/2016
//
// Project: Neural Network Accelerator
// Module:  Neural Network Accelerator Testbench
// Test:    Comparison Against Trained Neural Network
//

module nna_tb;

// ============== Inputs Memory Memory ==============
    parameter sample_size = 2097;
    
    reg [31:0] sample_mem [sample_size:0];
    
    initial begin
        $readmemb("sig_samples.bin", sample_mem);
    end

// ============== NNA Memory ==============

// ===== Memory Iteration =====
integer i, j, outputs;

// ===== Reset & Clock =====
reg rst;
reg clk;                                       // 200 MHz clock

// ===== Clock and delay settings =====
parameter delay = 6;          // One entire period
parameter half_per = delay/2; // Half period
parameter ini = delay-0.3;    // setup time of 0.3

always begin
   #(half_per) clk <= ~clk;
end


// ===== Neural Network Accelerator =====
    wire [15:0] data_upper;
    wire [15:0] data_lower;
    
    wire [31:0] wmem_rd_data;
    wire [31:0] omem_rd_data;
    wire [31:0] bmem_rd_data;
    wire [15:0] config_mem_data;

    wire [15:0] dmem_mem_map_rd_data;   // NEED TO MAKE MUX FOR THIS BEFORE DMEM
    wire dmem_wr_mux_ctrl;
    wire dmem_wr_en_offload;
    wire dmem_offload_data_ctrl;
    
    wire bias_mem_wr_en;
    wire out_buf_mem_wr_en;
    wire weight_mem_wr_en;
    wire config_mem_wr_en;
    
    wire [13:0] weight_mem_addr;
    wire [9:0]  bias_mem_addr;
    wire [4:0]  out_buf_mem_addr;
    wire [2:0]  config_mem_addr;
    
    wire [31:0] omem_wr_data;


// ===== Memories =====
    // ===== Data Memory =====
    wire [15:0] nna_dmem_wr_addr;
    reg [15:0] alu_out = 16'h0000;
    reg [15:0] result = 16'h0000;
    
    reg dmem_wr_en;
    reg dmem_rd_en;
    wire [15:0] dmem_rd_data;
    
    wire [15:0] dmem_wr_data_final;
    wire [15:0] dmem_rd_data_final;
    wire [15:0] dmem_wr_addr_final;
    wire dmem_wr_en_final;
    
    assign dmem_rd_data_final = (alu_out[15:3] == 13'b1_1111_1111_1111)? dmem_mem_map_rd_data : dmem_rd_data;
    assign dmem_wr_data_final = (dmem_wr_mux_ctrl)? (dmem_offload_data_ctrl)? omem_rd_data[31:16] : omem_rd_data[15:0] : result;
    assign dmem_wr_addr_final = (dmem_wr_mux_ctrl)? nna_dmem_wr_addr : alu_out;
    assign dmem_wr_en_final = (dmem_wr_mux_ctrl)? dmem_wr_en_offload : dmem_wr_en;
    
    
// ===== Initialize test =====
initial begin
    outputs = $fopen("nna_outputs_sig.bin","w");
    
    // Assert reset
    rst = 1;
    clk = 0;
    // De-assert reset
    #(delay);
    rst = 0;
    #(delay + delay/2);
    dmem_wr_en = 0;
    dmem_rd_en = 0;
    alu_out = 16'h0000;
    result = 16'h0000;
    #(delay);
    
    // // ==== Begin Neural Network Accelerator ====
    // dmem_wr_en = 1;
    // dmem_rd_en = 0;
    // alu_out = 16'b1111_1111_1111_1000;
    // result = 16'b111_00_001000_0011_1;
    // 
    // #(delay);
    
    for ( i = 0; i <= 698; i++ ) begin
        
        // ==== Begin Neural Network Accelerator ====
        dmem_wr_en = 1;
        dmem_rd_en = 0;
        alu_out = 16'b1111_1111_1111_1000;
        result = 16'b111_01_001000_0010_1;
        
        #(delay);
    
        // ==== Keep Polling finish flag until NNA is done ====
        while (dmem_mem_map_rd_data != 16'h0001) begin
            dmem_wr_en = 0;
            dmem_rd_en = 1;
            alu_out = 16'b1111_1111_1111_1100;
            result = 16'b111_01_001000_0010_1;
            #(delay);
        end
    
        $fwrite(outputs, "%b\n", omem0.OMEM[16][7:0]);
    
        // Set Neural Network Begin Flag to 0
        dmem_wr_en = 1;
        dmem_rd_en = 0;
        alu_out = 16'b1111_1111_1111_1100;
        result = 16'b111_01_001000_0010_0;
        #(delay);
        
        // Set Opcode to Neural Network Reset (NN_RESET)
        dmem_wr_en = 1;
        dmem_rd_en = 0;
        alu_out = 16'b1111_1111_1111_1000;
        result = 16'b101_01_001000_0010_0;
        #(delay);
        
        // Set Opcode to NOOP
        dmem_wr_en = 1;
        dmem_rd_en = 0;
        alu_out = 16'b1111_1111_1111_1000;
        result = 16'b111_01_001000_0010_0;
        #(2*delay);
        
        
        for ( j = 0; j <= 2; j++ ) begin
            omem0.OMEM[j] = sample_mem[j+((i+1)*3)];
        end
    end
    
    // // Finish flag is detected, Load Address for Output Buffer Offload
    // dmem_wr_en = 1;
    // dmem_rd_en = 0;
    // alu_out = 16'b1111_1111_1111_1001; 
    // result = 16'b0000_0000_0000_0010;       // Start at address 2
    // #(delay);
    // 
    // // Load Command for Output Buffer Offload
    // dmem_wr_en = 1;
    // dmem_rd_en = 0;
    // alu_out = 16'b1111_1111_1111_1000;
    // result = 16'b100_00_000000_0100_0;
    // #(32*delay);
    // 
    // // Set Opcode to NOOP
    // dmem_wr_en = 1;
    // dmem_rd_en = 0;
    // alu_out = 16'b1111_1111_1111_1000;
    // result = 16'b111_00_000000_0100_0;
    // #(10*delay);
    
    #(20*delay);
    
    
    $finish;
end
   
// ============== Neural Network Accelerator ==============
   
nna nna0(
    .rst(rst),
    .clk(clk),
    
    .data_upper(data_upper),
    .data_lower(data_lower),
    
    .wmem_rd_data(wmem_rd_data),
    .omem_rd_data(omem_rd_data),
    .bmem_rd_data(bmem_rd_data),
    .config_mem_data(config_mem_data),

    .dmem_wr_en(dmem_wr_en),
    .dmem_rd_en(dmem_rd_en),
    .dmem_rd_addr(alu_out),                             // THIS WILL BE alu_out IN THE PROCESSOR
    .dmem_mem_map_rd_data(dmem_mem_map_rd_data),        // NEED TO MAKE MUX FOR THIS BEFORE DMEM
    .nna_dmem_wr_addr(nna_dmem_wr_addr),                    // THIS WILL BE alu_out IN THE PROCESSOR
    .dmem_wr_addr(alu_out),
    
    .dmem_wr_data(result),
    .dmem_wr_mux_ctrl(dmem_wr_mux_ctrl),
    .dmem_wr_en_offload(dmem_wr_en_offload),
    .dmem_offload_data_ctrl(dmem_offload_data_ctrl),
    
    .bias_mem_wr_en(bias_mem_wr_en),
    .out_buf_mem_wr_en(out_buf_mem_wr_en),
    .weight_mem_wr_en(weight_mem_wr_en),
    .config_mem_wr_en(config_mem_wr_en),
    
    .weight_mem_addr(weight_mem_addr),
    .bias_mem_addr(bias_mem_addr),
    .out_buf_mem_addr(out_buf_mem_addr),
    .config_mem_addr(config_mem_addr),
    
    .omem_wr_data(omem_wr_data)
    // MAKE SURE TO TIE OMEM_RD_DATA AS A POTENTIAL DMEM_WR_DATA SIGNAL
);


sig_wmem wmem0(
    .clk(clk),
    .rd_data(wmem_rd_data),
    .rd_en(weight_mem_wr_en),
    .wr_en(weight_mem_wr_en),
    .rd_addr(weight_mem_addr),
    .wr_addr(weight_mem_addr),
    .wr_data({data_upper, data_lower})
);

sig_cmem cmem0(
    .clk(clk),
    .rd_data(config_mem_data),
    .rd_en(config_mem_wr_en),
    .wr_en(config_mem_wr_en),
    .rd_addr(config_mem_addr),
    .wr_addr(config_mem_addr),
    .wr_data(data_lower)
);

sig_omem omem0(
    .clk(clk),
    .rd_data(omem_rd_data),
    .rd_en(out_buf_mem_wr_en),
    .wr_en(out_buf_mem_wr_en),
    .rd_addr(out_buf_mem_addr),
    .wr_addr(out_buf_mem_addr),
    .wr_data(omem_wr_data)
);

sig_bmem bmem0(
    .clk(clk),
    .rd_data(bmem_rd_data),
    .rd_en(bias_mem_wr_en),
    .wr_en(bias_mem_wr_en),
    .rd_addr(bias_mem_addr),
    .wr_addr(bias_mem_addr),
    .wr_data({data_upper, data_lower})
);

// ============== Data Memory ==============

sig_dmem dmem0(
      .clk(clk),
      .rd_en(dmem_rd_en),
      .wr_en(dmem_wr_en_final),
      .rd_addr(alu_out),
      .wr_addr(dmem_wr_addr_final), // MUX WR_ADDR AS WELL
      .wr_data(dmem_wr_data_final),  // MUX RESULT
      .rd_data(dmem_rd_data)
   );

endmodule
