// Name:    Lev Kurilenko
// Class:   EE 525
// Date:    2/15/2016
//
// Project: Neural Network Accelerator Enhancement
// Module:  Hardware Scheduler


// Documentation:
// Memory Mapped Data Memory for NNA
// -------------------------------------------------------------------------------
// | 15 | 14 | 13 | 12 | 11 | 10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 |     0     |
// -------------------------------------------------------------------------------
// |   OP Code    | ACtrl   |    input_layer_nodes   |   num_layers  | NN_start  |
// -------------------------------------------------------------------------------
// |                       Address                                               |
// -------------------------------------------------------------------------------
// |                        Data                                                 |
// -------------------------------------------------------------------------------
// |                        Data                                                 |
// -------------------------------------------------------------------------------
// |                                                                 | NN_finish |
// -------------------------------------------------------------------------------
//
//
// Configuration Memory Mapping (8 Addresses)
//      -----------------------------------------------------------------------
// Addr | 15 | 14 | 13 | 12 | 11 | 10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
//      -----------------------------------------------------------------------
// 0x00 |                   |   nodes_in_layer        |     nodes_in_layer    |
//      -----------------------------------------------------------------------
// 0x01 |                   |   nodes_in_layer        |     nodes_in_layer    |
//      -----------------------------------------------------------------------
// 0x02 |                   |   nodes_in_layer        |     nodes_in_layer    |
//      -----------------------------------------------------------------------
//
// Instructions
// * Weight Mem Write
// * Output Buffer Mem Write
// * Configuration Memory Write
// * Bias Memory Write
// * Output Buffer Offload
// * Neural Network Reset
// * NOOP
//
//
// WMW      = 3'b000
// OBMW     = 3'b001
// CMW      = 3'b010
// BMW      = 3'b011
// OBO      = 3'b100
// NN_RESET = 3'b101
// NOOP     = 3'b111
//


module hardware_scheduler (
    input rst,
    input clk,                                      // 200 MHz clock
    input [15:0] cmd,                               // Command data used for control
    input [15:0] addr,                              // Address for the various memories in Accelerator

    input [15:0] config_mem_data,                   // Configuration Memory Data
    
    output reg mac_en,                              // Enable signal for the MAC
    output reg mac_rst,                             // Reset signal for the MAC
    output [1:0] actv_ctrl,                         // Activation Control Output Signal
    
    output reg bias_ctrl,                           // Bias control for data input of Node
    output reg write_data_ctrl,                     // Mux control for Output Buffer Memory write data
    output [1:0] out_ctrl,                          // Output Buffer data control for 4-to-1 Mux

    output reg nn_finish,                           // Register storing finish flag for neural network
    
    output reg bias_mem_wr_en,                      // Bias Memory Write Enable
    output reg out_buf_mem_wr_en,                   // Output Buffer Memory Write Enable
    output reg weight_mem_wr_en,                    // Weight Memory Write Enable
    output reg config_mem_wr_en,                    // Configuration Memory Write Enable

    output [13:0] weight_mem_addr,                  // Weight Memory Address
    output [9:0]  bias_mem_addr,                    // Bias Memory Address
    output [4:0]  out_buf_mem_addr,                 // Output Buffer Memory Address
    output [2:0]  config_mem_addr,                  // Configuration Memory Addr

    output reg [15:0] nna_dmem_wr_addr,             // Data Memory Write Address
    output reg dmem_wr_mux_ctrl,                    // Data Memory Addresses and Data Mux Control
    output reg dmem_wr_en_offload,                  // Write Enable control for offload command
    output dmem_offload_data_ctrl                   // Control weather upper or lower bits are written
    
    // DATA MAY JUST BE ROUTED DIRECTLY INTO ALL OF THE MEMORIES WITH MUX AND WR_EN CONTROL
    // THIS SHOULD BE UPDATED IN DATAPATH TO REFLECT THE NEW DESIGN
    //input [32:0] data
);

// Neural Network Begin States
// UNCOMMENT ENUM BELOW WHEN SIMULATING
// enum { IDLE, PRESENT_INPUT, UPDATE_INPUT, PRESENT_BIAS, ACTIVATION, WRITE_OUTPUT, MAC_RESET, NEXT_LAYER, NN_FINISH } state;

// UNCOMMENT STATE CODE BELOW WHEN SYNTHESIZING
localparam IDLE            = 4'b0000;
localparam PRESENT_INPUT   = 4'b0001;
localparam UPDATE_INPUT    = 4'b0010;
localparam PRESENT_BIAS    = 4'b0011;
localparam ACTIVATION      = 4'b0100;
localparam WRITE_OUTPUT    = 4'b0101;
localparam MAC_RESET       = 4'b0110;
localparam NEXT_LAYER      = 4'b0111;
localparam NN_FINISH       = 4'b1000;
reg [3:0]  state;                       // Neural Network State

// Instruction OP Codes
localparam WMW      = 3'b000;           // Weight Memory Write
localparam OBMW     = 3'b001;           // Output Buffer Memory Write
localparam CMW      = 3'b010;           // Configuration Memory Write
localparam BMW      = 3'b011;           // Bias Memory Write
localparam OBO      = 3'b100;           // Output Buffer Offload
localparam NN_RESET = 3'b101;           // Neural Network Reset
localparam NOOP     = 3'b111;           // No Op

wire [2:0] op_code;                     // Op Code for Hardware Scheduler Commands
wire nn_begin;                          // Control signal to begin Neural Network Computation

reg [2:0] next_op_code;                 // Op code used after Op code fetch

reg [6:0] tot_node_cnt;                 // Counter keeps track of total nodes iterated in layer
reg [6:0] input_cnt;                    // Counter to iterate through inputs

wire [3:0] num_layers;                  // Stores number of layers, max = 16
reg [4:0] current_layer;                // Stores the value of the current layer
reg [5:0] nodes_in_layer;               // # of nodes in current layer, max = 64
reg [5:0] pre_layer_nodes;              // # of nodes in previous layer, used as input # value for current layer

reg [13:0] weight_mem_addr_ptr;         // Weight Memory Address
reg [9:0]  bias_mem_addr_ptr;           // Bias Memory Address

wire [4:0] out_buf_mem_addr_ptr;        // Output Buffer Memory Address
reg [3:0]  out_buf_mem_addr_ptr_output; // Two pointers are needed for Output Buffer Memory becuase
reg [3:0]  out_buf_mem_addr_ptr_input;  // there needs to be a seperate pointer dedicated for output and input
reg final_layer_flag;                   // Flag used to record if final layer is odd or even
reg inhib_op_code_flag;                 // Flag set to inhibit OP code decode until Output Buffer Offload is finished

// Muxes for addresses of NNA Memories
assign weight_mem_addr = (nn_begin)? weight_mem_addr_ptr : addr[13:0];
assign bias_mem_addr = (nn_begin)? bias_mem_addr_ptr : addr[9:0];
assign out_buf_mem_addr = (nn_begin || ((next_op_code == OBO) && (nn_finish == 1)))? out_buf_mem_addr_ptr : addr[4:0];
assign config_mem_addr = (nn_begin)? current_layer[3:1] : addr[2:0];        // Uses current_layer/2 as address

assign out_buf_mem_addr_ptr = (out_buf_mem_wr_en)? {!current_layer[0], out_buf_mem_addr_ptr_output} : {current_layer[0], out_buf_mem_addr_ptr_input};

assign num_layers = cmd[4:1];                   // Assign proper cmd bits to num_layers
assign op_code = cmd[15:13];                    // Assign proper cmd bits to op_code
assign nn_begin = cmd[0];                       // Assign proper cmd bit to nn_begin
assign out_ctrl = input_cnt[1:0];               // Assign output control to 2 most LSB of input_cnt
assign actv_ctrl = cmd[12:11];                  // Assign proper cmd bit to actv_ctrl
assign dmem_offload_data_ctrl = input_cnt[0];   // Assign data_memory_output_mux control to input count

always @(posedge clk, posedge rst) begin
    if (rst) begin
        tot_node_cnt <= 7'h00;
        current_layer <= 5'h0;
        input_cnt <= 7'h00;
        nodes_in_layer <= config_mem_data[5:0];
        pre_layer_nodes <= cmd[10:5];
        bias_ctrl <= 1'b0;
        final_layer_flag <= 1'b0;
        weight_mem_addr_ptr <= 14'h0000; 
        bias_mem_addr_ptr <= 10'h000;
        out_buf_mem_addr_ptr_output <= 4'h0;
        out_buf_mem_addr_ptr_input <= 4'h0;
        out_buf_mem_wr_en <= 1'b0;
        state <= IDLE;
        mac_en <= 0;
        mac_rst <= 1;
        nn_finish <= 0;
        nna_dmem_wr_addr <= 16'h0000;
        dmem_wr_mux_ctrl <= 1'b0;
        inhib_op_code_flag <= 1'b0;
        next_op_code <= NOOP;
    end
    else if (!nn_begin) begin         // Neural Network has not been activated
        // out_buf_mem_wr_en <= 1'b0;
        
        if ((next_op_code != OBO) && (inhib_op_code_flag == 1'b0)) begin
            next_op_code <= op_code;
            if (next_op_code == NN_RESET) begin
                nn_finish <= 0;
            end
            
            if (op_code == OBMW) begin
                out_buf_mem_wr_en <= 1'b1;
            end
            else if (op_code == OBO) begin
                inhib_op_code_flag <= 1'b1;
            end
        end
        else if (next_op_code == OBO) begin
            out_buf_mem_wr_en <= 1'b0;
            dmem_wr_en_offload <= 1'b1;
            nna_dmem_wr_addr <= addr;
            
            if (input_cnt == 0) begin
                dmem_wr_mux_ctrl <= 1'b1;      // Set data memory address and address data mux
                if (dmem_wr_mux_ctrl == 1) begin
                    input_cnt <= input_cnt + 1;
                    nna_dmem_wr_addr <= nna_dmem_wr_addr + 1;
                end
                current_layer[0] <= final_layer_flag;
                out_buf_mem_addr_ptr_input <= 4'h0;
            end
            else if (input_cnt <= 31) begin
                input_cnt <= input_cnt + 1;
                nna_dmem_wr_addr <= nna_dmem_wr_addr + 1;
                
                if (input_cnt[0] == 1) begin
                    out_buf_mem_addr_ptr_input <= out_buf_mem_addr_ptr_input + 1;
                end
            end
            else begin
                inhib_op_code_flag <= 1'b0;
                dmem_wr_en_offload <= 1'b0;     // CHECK LATER
                dmem_wr_mux_ctrl <= 1'b0;
                input_cnt <= 0;
                out_buf_mem_addr_ptr_input <= 4'h0;
                next_op_code <= NOOP;
            end
        end
    end
    else if (nn_begin && (nn_finish == 0)) begin          // Neural Network has been activated
    // states: IDLE, PRESENT_INPUT, UPDATE_INPUT, PRESENT_BIAS, ACTIVATION, WRITE_OUTPUT, NEXT_LAYER, NN_FINISH    
        case (state)
            IDLE: begin
                tot_node_cnt <= 7'h00;
                current_layer <= 5'h0;
                input_cnt <= 7'h00;
                nodes_in_layer <= config_mem_data[5:0];
                pre_layer_nodes <= cmd[10:5];
                bias_ctrl <= 1'b0;
                final_layer_flag <= 1'b0;
                weight_mem_addr_ptr <= 14'h0000; 
                bias_mem_addr_ptr <= 10'h000;
                out_buf_mem_addr_ptr_output <= 4'h0;
                out_buf_mem_addr_ptr_input <= 4'h0;
                out_buf_mem_wr_en <= 1'b0;
                next_op_code <= NOOP;
                mac_en <= 1;
                mac_rst <= 0;
                if (nn_finish == 1'b0) begin
                    state <= PRESENT_INPUT;
                end
            end
            PRESENT_INPUT: begin
                mac_rst <= 0;
                input_cnt <= input_cnt + 1;
                weight_mem_addr_ptr <= weight_mem_addr_ptr + 1;     // Increment Weight Address
                
                // Seperate read and write pointers
                // OUTPUT_ADDR = {!current_layer[0], output_ptr[3:0]
                // INPUT_ADDR = {current_layer[0], input_ptr[3:0]}
                
                if (input_cnt == pre_layer_nodes) begin
                    tot_node_cnt <= tot_node_cnt + 4;
                    input_cnt <= 7'h00;
                    bias_ctrl <= 1'b1;
                    //weight_mem_addr_ptr <= weight_mem_addr_ptr + 1;     // Increment Weight Address
                    out_buf_mem_addr_ptr_input <= 4'h0;
                    state <= PRESENT_BIAS;
                end
                else if (input_cnt[1:0] == 3) begin
                    //weight_mem_addr_ptr <= weight_mem_addr_ptr + 1;     // Increment Weight Address
                    out_buf_mem_addr_ptr_input <= out_buf_mem_addr_ptr_input + 1;
                    out_buf_mem_wr_en <= 1'b0;
                    state <= UPDATE_INPUT;
                    mac_en <= 0;
                end
            end
            UPDATE_INPUT: begin
                mac_en <= 1;
                state <= PRESENT_INPUT;
            end
            PRESENT_BIAS: begin
                bias_ctrl <= 1'b0;
                bias_mem_addr_ptr <= bias_mem_addr_ptr + 1;
                state <= ACTIVATION;
                mac_en <= 0;
            end
            ACTIVATION: begin
                out_buf_mem_wr_en <= 1'b1;
                state <= WRITE_OUTPUT;
            end
            WRITE_OUTPUT: begin
                out_buf_mem_wr_en <= 1'b0;
                mac_rst <= 1'b1;
                out_buf_mem_addr_ptr_output <= out_buf_mem_addr_ptr_output + 1;
                if (tot_node_cnt >= (nodes_in_layer + 1)) begin
                    pre_layer_nodes <= nodes_in_layer;
                    current_layer <= current_layer + 1;
                    state <= NEXT_LAYER;
                end
                else begin
                    state <= MAC_RESET;
                    mac_en <= 1;
                end
            end
            MAC_RESET: begin
                mac_rst <= 1'b0;
                state <= PRESENT_INPUT;
            end
            NEXT_LAYER: begin
                mac_rst <= 1'b0;
                if (current_layer[0]) begin
                    nodes_in_layer <= config_mem_data[11:6];
                end
                else if (!current_layer[0]) begin    // Addition to current_layer will move config_mem_addr by one
                    nodes_in_layer <= config_mem_data[5:0];
                end
                
                if (current_layer == (num_layers + 1)) begin
                    state <= NN_FINISH;
                end
                else begin
                    mac_en <= 1;
                    input_cnt <= 7'h00;
                    tot_node_cnt <= 7'h00;
                    out_buf_mem_addr_ptr_output <= 4'h0;
                    out_buf_mem_addr_ptr_input <= 4'h0;
                    state <= PRESENT_INPUT;
                end
            end
            NN_FINISH: begin
                tot_node_cnt <= 7'h00;
                current_layer <= 5'h0;
                final_layer_flag <= current_layer[0];
                input_cnt <= 7'h00;
                bias_ctrl <= 1'b0;
                weight_mem_addr_ptr <= 14'h0000; 
                bias_mem_addr_ptr <= 10'h000;
                out_buf_mem_addr_ptr_output <= 4'h0;
                out_buf_mem_addr_ptr_input <= 4'h0;
                mac_rst <= 1;
                mac_en <= 0;
                nn_finish <= 1'b1;
                // WRITE FINISH FLAG TO WR ENABLE
                
                state <= IDLE;
            end
            default: begin
                tot_node_cnt <= 7'h00;
                current_layer <= 5'h0;
                input_cnt <= 7'h00;
                nodes_in_layer <= config_mem_data[5:0];
                pre_layer_nodes <= cmd[10:5];
                bias_ctrl <= 1'b0;
                final_layer_flag <= 1'b0;
                weight_mem_addr_ptr <= 14'h0000; 
                bias_mem_addr_ptr <= 10'h000;
                out_buf_mem_addr_ptr_output <= 4'h0;
                out_buf_mem_addr_ptr_input <= 4'h0;
                out_buf_mem_wr_en <= 1'b0;
                state <= IDLE;
                mac_en <= 0;
                mac_rst <= 1;
                nn_finish <= 0;
                nna_dmem_wr_addr <= 16'h0000;
                dmem_wr_mux_ctrl <= 1'b0;
                inhib_op_code_flag <= 1'b0;
                next_op_code <= NOOP;
            end
        endcase
    end
    // else begin
    //     tot_node_cnt <= 7'h00;
    //     current_layer <= 5'h0;
    //     input_cnt <= 7'h00;
    //     nodes_in_layer <= config_mem_data[5:0];
    //     pre_layer_nodes <= cmd[10:5];
    //     bias_ctrl <= 1'b0;
    //     final_layer_flag <= 1'b0;
    //     weight_mem_addr_ptr <= 14'h0000; 
    //     bias_mem_addr_ptr <= 10'h000;
    //     out_buf_mem_addr_ptr_output <= 4'h0;
    //     out_buf_mem_addr_ptr_input <= 4'h0;
    //     out_buf_mem_wr_en <= 1'b0;
    //     state <= IDLE;
    //     mac_en <= 0;
    //     mac_rst <= 1;
    //     // nn_finish <= 0;
    //     nna_dmem_wr_addr <= 16'h0000;
    //     dmem_wr_mux_ctrl <= 1'b0;
    //     inhib_op_code_flag <= 1'b0;
    //     next_op_code <= NOOP;
    // end
end


always @(*) begin
    casez (next_op_code)
        WMW:  begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b1;
            config_mem_wr_en    = 1'b0;
        end
        OBMW: begin
            write_data_ctrl     = 1'b1;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b0;
        end
        CMW:  begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b1;
        end
        BMW:  begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b1;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b0;
        end
        OBO: begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b0;
        end
        NOOP: begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b0;
        end
        default: begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b0;
        end
    endcase
end

endmodule
