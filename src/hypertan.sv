module hypertan #(
   parameter weight_width = 8,
   parameter data_width = 8,
   parameter output_width = 8
   ) (
   input [weight_width+data_width-1:0] operand,
   output logic [output_width-1:0] result
);

always_comb begin
   if (operand[15] & ~&operand[14:11]) begin  // if operand <= -64
      result = 8'b1_11_00000;   // Set output to -1
   end
   else if (~operand[15] & |operand[14:11]) begin  // >= 64
      result = 8'b0_01_00000;   // Set output to 1
   end
   else begin
      result = {operand[12], operand[12:6]};  // divide by 2
   end
end

endmodule
