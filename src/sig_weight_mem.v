// Weight Memory
// - 32-bits wide, 16K locations

module sig_wmem #(
    parameter WMEM_SIZE = 16'h3FFF
)(
    output reg [31:0] rd_data, // Read data
    input 	          rd_en,   // Read enable (high true)
    input 	          wr_en,   // Write enable (high true)
    input [13:0]      rd_addr, // Read address
    input [13:0]      wr_addr, // Write address
    input [31:0]      wr_data, // Write data
    input 	          clk      // Clock
);

    // Memory declaration
    reg [31:0] WMEM [WMEM_SIZE:0];

    initial begin
        $readmemb("sig_weights.bin", WMEM);
    end
    
    // Write behavior
    always @(posedge clk)  begin
        if (wr_en) begin
            WMEM[wr_addr] <= wr_data;
        end
    end

    // Static delay for reads
    localparam output_del = 0.2;

    // Read behavior
    always @(*) begin
        #(output_del) rd_data = (!rd_en)? WMEM[rd_addr]:rd_data;
    end

endmodule
