// Bias Memory
// - 32-bits wide, 1024 locations

module sig_bmem #(
    parameter BMEM_SIZE = 16'hffff
)(
    output reg [31:0] rd_data, // Read data
    input 	          rd_en,   // Read enable (high true)
    input 	          wr_en,   // Write enable (high true)
    input [9:0]      rd_addr, // Read address
    input [9:0]      wr_addr, // Write address
    input [31:0]      wr_data, // Write data
    input 	          clk      // Clock
);

    // Memory declaration
    reg [31:0] BMEM [BMEM_SIZE:0];

    initial begin
        $readmemb("sig_biases.bin", BMEM);
    end
    
    // Write behavior
    always @(posedge clk)  begin
        if (wr_en) begin
            BMEM[wr_addr] <= wr_data;
        end
    end

    // Static delay for reads
    localparam output_del = 0.2;

    // Read behavior
    always @(*) begin
        #(output_del) rd_data = (!rd_en)? BMEM[rd_addr]:rd_data;
    end

endmodule
