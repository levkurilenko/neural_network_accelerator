// Name:    Lev Kurilenko
// Class:   EE 525
// Date:    3/2/2016
//
// Project: Neural Network Accelerator
// Module:  Neural Network Accelerator
//
//
// Addresses corresponding to 
// memory mapped portions of 
// Data Memory
// -----------------------
// | Addr | Mapping      |
// -------+---------------
// | 0x04 |   nn_finish  |
// -------+---------------
// | 0x03 |   data_lower |
// -------+---------------
// | 0x02 |   data_upper |
// -------+---------------
// | 0x01 |   addr       |
// -------+---------------
// | 0x00 |   cmd        |
// -----------------------


module nna(
    input rst,
    input clk,
    
    output reg [15:0] data_upper,
    output reg [15:0] data_lower,
    
    input [31:0] wmem_rd_data,
    input [31:0] omem_rd_data,
    input [31:0] bmem_rd_data,
    input [15:0] config_mem_data,
    
    input dmem_wr_en,
    input dmem_rd_en,
    input [15:0] dmem_rd_addr,                  // THIS WILL BE alu_out IN THE PROCESSOR
    // output reg [15:0] dmem_mem_map_rd_data,     // NEED TO MAKE MUX FOR THIS BEFORE DMEM
    output [15:0] dmem_mem_map_rd_data,     // NEED TO MAKE MUX FOR THIS BEFORE DMEM
    output [15:0] nna_dmem_wr_addr,             // THIS WILL BE alu_out IN THE PROCESSOR
    input [15:0] dmem_wr_addr,
    
    input [15:0] dmem_wr_data,
    output dmem_wr_mux_ctrl,
    output dmem_wr_en_offload,
    output dmem_offload_data_ctrl,
    
    output bias_mem_wr_en,
    output out_buf_mem_wr_en,
    output weight_mem_wr_en,
    output config_mem_wr_en,

    output [13:0] weight_mem_addr,
    output [9:0]  bias_mem_addr,
    output [4:0]  out_buf_mem_addr,
    output [2:0]  config_mem_addr,
    
    output [31:0] omem_wr_data
    
    // MAKE SURE TO TIE OMEM_RD_DATA AS A POTENTIAL DMEM_WR_DATA SIGNAL
);

// ===== Data Memory Mapped =====
reg [15:0] cmd;
reg [15:0] addr;
// reg [15:0] data_upper;
// reg [15:0] data_lower;

// ===== Neuron =====
parameter weight_width = 8;
parameter data_width = 8;
parameter output_width = 8;

wire [output_width-1:0] n0_result;
wire [output_width-1:0] n1_result;
wire [output_width-1:0] n2_result;
wire [output_width-1:0] n3_result;

// ===== Muxes =====
wire [weight_width-1:0] weight0;
wire [weight_width-1:0] weight1;
wire [weight_width-1:0] weight2;
wire [weight_width-1:0] weight3;

wire [data_width-1:0] buffer_output;
wire [data_width-1:0] data_input;       // Neuron Data Input

// wire [31:0] omem_wr_data;   // Output Buffer Write Data

// ===== Memory Outputs =====
// wire [31:0] wmem_rd_data;
// wire [31:0] omem_rd_data;
// wire [31:0] bmem_rd_data;


// ===== Hardware Scheduler =====
wire mac_en;
wire mac_rst;
wire [1:0] actv_ctrl;

wire bias_ctrl;                                 // Bias control for data input of Node
wire write_data_ctrl;                           // Mux control for Output Buffer Memory write data
wire [1:0] out_ctrl;                            // Output Buffer data control for 4-to-1 Mux

wire nn_finish;                                 // finish flag from neural network

// wire bias_mem_wr_en;                            // Bias Memory Write Enable
// wire out_buf_mem_wr_en;                         // Output Buffer Memory Write Enable
// wire weight_mem_wr_en;                          // Weight Memory Write Enable
// wire config_mem_wr_en;                          // Configuration Memory Write Enable

// wire [13:0] weight_mem_addr;                    // Weight Memory Address
// wire [9:0]  bias_mem_addr;                      // Bias Memory Address
// wire [4:0]  out_buf_mem_addr;                   // Output Buffer Memory Address
// wire [2:0]  config_mem_addr;                    // Configuration Memory Addr


assign weight0 = (bias_ctrl)? bmem_rd_data[7:0] : wmem_rd_data[7:0];
assign weight1 = (bias_ctrl)? bmem_rd_data[15:8] : wmem_rd_data[15:8];
assign weight2 = (bias_ctrl)? bmem_rd_data[23:16] : wmem_rd_data[23:16];
assign weight3 = (bias_ctrl)? bmem_rd_data[31:24] : wmem_rd_data[31:24];

assign buffer_output = (out_ctrl == 0)? omem_rd_data[7:0]: (out_ctrl == 1)? omem_rd_data[15:8] : (out_ctrl == 2)?omem_rd_data[23:16] : omem_rd_data[31:24];
assign data_input = (bias_ctrl)? 8'b001_00000 : buffer_output;

assign omem_wr_data = (write_data_ctrl)? {data_upper, data_lower} : {n3_result, n2_result, n1_result, n0_result};

assign dmem_mem_map_rd_data = {15'h0000, nn_finish};

always @(posedge clk, posedge rst) begin
    
    // Memory Mapped DMEM Write Functionality
    if (rst) begin
        cmd <= 16'b111_00_000000_0000_0;
        addr <= 16'h0000;
        data_upper <= 16'h0000;
        data_lower <= 16'h0000;
    end
    else if ((dmem_wr_en == 1'b1) && (dmem_wr_addr[15:3] == 13'b1_1111_1111_1111)) begin
        if (dmem_wr_addr[2:0] == 3'b000) begin
            cmd <= dmem_wr_data;
        end
        else if (dmem_wr_addr[2:0] == 3'b001) begin
            addr <= dmem_wr_data;
        end
        else if (dmem_wr_addr[2:0] == 3'b010) begin
            data_upper <= dmem_wr_data;
        end
        else if (dmem_wr_addr[2:0] == 3'b011) begin
            data_lower <= dmem_wr_data;
        end
    end
end

// Memory Mapped DMEM Finish Flag Read Functionality
//always @(*) begin
    //dmem_mem_map_rd_data = (dmem_rd_en && (dmem_rd_addr[15:0] == 16'hFFFC))? {15'h0000, nn_finish} : 16'h0000;
//end


hardware_scheduler hardware_scheduler0(
    .rst(rst),
    .clk(clk),
    .cmd(cmd),
    .addr(addr),

    .config_mem_data(config_mem_data),
    
    .mac_en(mac_en),
    .mac_rst(mac_rst),
    .actv_ctrl(actv_ctrl),
    
    .nn_finish(nn_finish),
    
    .bias_ctrl(bias_ctrl),
    .write_data_ctrl(write_data_ctrl),
    .out_ctrl(out_ctrl),

    .bias_mem_wr_en(bias_mem_wr_en),
    .out_buf_mem_wr_en(out_buf_mem_wr_en),
    .weight_mem_wr_en(weight_mem_wr_en),
    .config_mem_wr_en(config_mem_wr_en),

    .weight_mem_addr(weight_mem_addr),
    .bias_mem_addr(bias_mem_addr),
    .out_buf_mem_addr(out_buf_mem_addr),
    .config_mem_addr(config_mem_addr),
    
    .nna_dmem_wr_addr(nna_dmem_wr_addr),
    .dmem_wr_mux_ctrl(dmem_wr_mux_ctrl),
    .dmem_wr_en_offload(dmem_wr_en_offload),
    .dmem_offload_data_ctrl(dmem_offload_data_ctrl)
    
    // DATA MAY JUST BE ROUTED DIRECTLY INTO ALL OF THE MEMORIES WITH MUX AND WR_EN CONTROL
    // THIS SHOULD BE UPDATED IN DATAPATH TO REFLECT THE NEW DESIGN
);

// ============== NNA Neurons 0 through 3 ==============
neuron #(
    .weight_width(weight_width),
    .data_width(data_width),
    .output_width(output_width)) 
    n0 (
    .clk(clk),
    .rst(mac_rst),
    .en(mac_en),
    .activation(actv_ctrl),
    .weight(weight0),
    .data(data_input),
    .result(n0_result)
);

neuron #(
    .weight_width(weight_width),
    .data_width(data_width),
    .output_width(output_width)) 
    n1 (
    .clk(clk),
    .rst(mac_rst),
    .en(mac_en),
    .activation(actv_ctrl),
    .weight(weight1),
    .data(data_input),
    .result(n1_result)
);

neuron #(
    .weight_width(weight_width),
    .data_width(data_width),
    .output_width(output_width)) 
    n2 (
    .clk(clk),
    .rst(mac_rst),
    .en(mac_en),
    .activation(actv_ctrl),
    .weight(weight2),
    .data(data_input),
    .result(n2_result)
);

neuron #(
    .weight_width(weight_width),
    .data_width(data_width),
    .output_width(output_width)) 
    n3 (
    .clk(clk),
    .rst(mac_rst),
    .en(mac_en),
    .activation(actv_ctrl),
    .weight(weight3),
    .data(data_input),
    .result(n3_result)
);

endmodule
