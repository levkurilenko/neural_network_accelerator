// EE477 baseline data memory model
// - 16-bits wide, 64K locations

module dmem #(
    parameter DMEM_SIZE = 16'hffff
)(
    output reg [15:0] rd_data, // Read data
    input 	          rd_en,   // Read enable (high true)
    input 	          wr_en,   // Write enable (high true)
    input [15:0]      rd_addr, // Read address
    input [15:0]      wr_addr, // Write address
    input [15:0]      wr_data, // Write data
    input 	          clk      // Clock
);

    // Memory declaration
    reg [15:0] DMEM [DMEM_SIZE:0];

    // Write behavior
    always @(posedge clk)  begin
        if (wr_en) begin
            DMEM[wr_addr] <= wr_data;
        end
    end

    // Static delay for reads
    localparam output_del = 0.2;

    // Read behavior
    always @(*) begin
        #(output_del) rd_data = (rd_en)? DMEM[rd_addr]:rd_data;
    end

endmodule
