module relu #(
   parameter weight_width = 8,
   parameter data_width = 8,
   parameter output_width = 8
   ) (
   input [weight_width+data_width-1:0] operand,
   output logic [output_width-1:0] result
);

always_comb begin
   if (operand[15]) begin  // negative
      result = 'b0;
   end
   else if (|operand[14:12]) begin
      result = 'h7F;
   end
   else begin
      result = operand[12:5];
   end
end

endmodule
