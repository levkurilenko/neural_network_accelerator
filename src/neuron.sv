module neuron #(
   parameter weight_width = 8,
   parameter data_width = 8,
   parameter output_width = 8
   ) (
   input clk, rst, en,
   input [1:0] activation,
   input [weight_width-1:0] weight,
   input [data_width-1:0] data,
   output logic [output_width-1:0] result
);

logic [weight_width+data_width-1:0] MAC;
logic [weight_width+data_width-1:0] MAC_i;

logic [output_width-1:0] 
   result_relu, 
   result_sigmoid, 
   result_hypertan;

dw02_mac_sim #(
   .A_width(weight_width),
   .B_width(data_width)
) MAC_inst (
   .A(weight),
   .B(data),
   .C(MAC),
   .TC(1'b1),
   .MAC(MAC_i)
);

relu activation_a (
   .operand(MAC),
   .result(result_relu)
);

sigmoid activation_b (
   .operand(MAC),
   .result(result_sigmoid)
);

hypertan activation_c (
   .operand(MAC),
   .result(result_hypertan)
);

always_ff @(posedge clk or posedge rst) begin
   if (rst) begin
      MAC <= 'b0;
   end
   else if (en) begin
      MAC <= MAC_i;
   end
end

always_comb begin
   case (activation)
      2'b00: result = result_relu;
      2'b01: result = result_sigmoid;
      2'b10: result = result_hypertan;
      
      default: result = 8'h00;
   endcase
end

endmodule
