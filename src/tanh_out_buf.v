// Output Buffer Memory
// - 32-bits wide,  32 locations

module tanh_omem #(
    parameter OMEM_SIZE = 16'h001F
)(
    output reg [31:0] rd_data, // Read data
    input 	          rd_en,   // Read enable (high true)
    input 	          wr_en,   // Write enable (high true)
    input [4:0]      rd_addr, // Read address
    input [4:0]      wr_addr, // Write address
    input [31:0]      wr_data, // Write data
    input 	          clk      // Clock
);

    // Memory declaration
    reg [31:0] OMEM [OMEM_SIZE:0];

    initial begin
        $readmemb("tanh_samples.bin", OMEM);
    end
    
    // Write behavior
    always @(posedge clk)  begin
        if (wr_en) begin
            OMEM[wr_addr] <= wr_data;
        end
    end

    // Static delay for reads
    localparam output_del = 0.2;

    // Read behavior
    always @(*) begin
        #(output_del) rd_data = (!rd_en)? OMEM[rd_addr]:rd_data;
    end

endmodule
