// Configuration Memory
// - 16-bits wide, 8 locations

module sig_cmem #(
    parameter CMEM_SIZE = 16'h0007
)(
    output reg [15:0] rd_data, // Read data
    input 	          rd_en,   // Read enable (high true)
    input 	          wr_en,   // Write enable (high true)
    input [2:0]      rd_addr, // Read address
    input [2:0]      wr_addr, // Write address
    input [15:0]      wr_data, // Write data
    input 	          clk      // Clock
);

    // Memory declaration
    reg [15:0] CMEM [CMEM_SIZE:0];

    initial begin
        $readmemb("sig_config.bin", CMEM);
    end
    
    // Write behavior
    always @(posedge clk)  begin
        if (wr_en) begin
            CMEM[wr_addr] <= wr_data;
        end
    end

    // Static delay for reads
    localparam output_del = 0.2;

    // Read behavior
    always @(*) begin
        #(output_del) rd_data = (!rd_en)? CMEM[rd_addr]:rd_data;
    end

endmodule
