module dw02_mac_sim #(
   A_width = 8, 
   B_width = 8
   ) (
   input [A_width-1:0] A,
   input [B_width-1:0] B,
   input [A_width+B_width-1:0] C,
   input TC,
   output logic [A_width+B_width-1:0] MAC
);

always_comb begin
   if (~TC) begin
      MAC = A * B + C;
   end
   else begin
      MAC = {{(2*B_width+A_width){A[A_width-1]}}, A} *
            {{(2*A_width+B_width){B[B_width-1]}}, B} +
            C;
   end
end

endmodule
