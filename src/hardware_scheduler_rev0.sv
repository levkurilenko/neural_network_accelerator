// Name:    Lev Kurilenko
// Class:   EE 525
// Date:    2/15/2016
//
// Project: Neural Network Accelerator Enhancement
// Module:  Hardware Scheduler


// Documentation:
// Memory Mapped Data Memory for NNA
// -------------------------------------------------------------------------------
// | 15 | 14 | 13 | 12 | 11 | 10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 |     0     |
// -------------------------------------------------------------------------------
// |   OP Code    |ADD ACtrl|    input_layer_nodes   |   num_layers  | NN_start  |
// -------------------------------------------------------------------------------
// |                       Address                                               |
// -------------------------------------------------------------------------------
// |                        Data                                                 |
// -------------------------------------------------------------------------------
// |                        Data                                                 |
// -------------------------------------------------------------------------------
//
// Configuration Memory Mapping (8 Addresses)
//      -----------------------------------------------------------------------
// Addr | 15 | 14 | 13 | 12 | 11 | 10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
//      -----------------------------------------------------------------------
// 0x00 |                   |   nodes_in_layer        |     nodes_in_layer    |
//      -----------------------------------------------------------------------
// 0x01 |                   |   nodes_in_layer        |     nodes_in_layer    |
//      -----------------------------------------------------------------------
// 0x02 |                   |   nodes_in_layer        |     nodes_in_layer    |
//      -----------------------------------------------------------------------
//
//
// Instructions
// * Weight Mem Write
// * Output Buffer Mem Write
// * Configuration Memory Write
// * Bias Memory Write
// * Output Buffer Offload
//

module hardware_scheduler (
    input rst,
    input clk,                                      // 200 MHz clock
    input [15:0] cmd,                               // Command data used for control
    input [15:0] addr,                              // Address for the various memories in Accelerator

    input [15:0] config_mem_data,                   // Configuration Memory Data
    
    output reg mac_en,                              // Enable signal for the MAC
    output reg [1:0] actv_ctrl,                     // Activation Control Output Signal
    
    output reg bias_ctrl,                           // Bias control for data input of Node
    output reg write_data_ctrl,                     // Mux control for Output Buffer Memory write data
    output [1:0] out_ctrl,                          // Output Buffer data control for 4-to-1 Mux

    output reg bias_mem_wr_en,                      // Bias Memory Write Enable
    output reg out_buf_mem_wr_en,                   // Output Buffer Memory Write Enable
    output reg weight_mem_wr_en,                    // Weight Memory Write Enable
    output reg config_mem_wr_en,                    // Configuration Memory Write Enable

    output [13:0] weight_mem_addr,                  // Weight Memory Address
    output [9:0]  bias_mem_addr,                    // Bias Memory Address
    output [4:0]  out_buf_mem_addr,                 // Output Buffer Memory Address
    output [2:0]  config_mem_addr                   // Configuration Memory Addr

    // DATA MAY JUST BE ROUTED DIRECTLY INTO ALL OF THE MEMORIES WITH MUX AND WR_EN CONTROL
    // THIS SHOULD BE UPDATED IN DATAPATH TO REFLECT THE NEW DESIGN
    //input [32:0] data
);

// Neural Network Begin States
// MAY WANT TO ENUMERATE THESE
// IDLE, PRESENT_INPUT, UPDATE_INPUT, WRITE_OUTPUT, NEXT_LAYER, NN_FINISH

localparam IDLE            = 3'b000;
localparam PRESENT_INPUT   = 3'b001;
localparam UPDATE_INPUT    = 3'b010;
localparam WRITE_OUTPUT    = 3'b011;
localparam NEXT_LAYER      = 3'b100;
localparam NN_FINISH       = 3'b101;


// Instruction OP Codes
localparam WMW      = 3'b000;       // Weight Memory Write
localparam OBMW     = 3'b001;       // Output Buffer Memory Write
localparam CMW      = 3'b010;       // Configuration Memory Write
localparam BMW      = 3'b011;       // Bias Memory Write
//============================================================
// MAY REQUIRE MULTIPLE CLOCK CYCLES TO EXECUTE
localparam OBO      = 3'b100;       // Output Buffer Offload
//============================================================
localparam NN_START = 3'b101;       // Put Hardware Scheduler in Neural Network Start state
localparam NOOP     = 3'b111;       // No Op


wire [2:0] op_code;                     // Op Code for Hardware Scheduler Commands
wire nn_begin;                          // Control signal to begin Neural Network Computation

reg [2:0] next_op_code;                 // Op code used after Op code fetch
reg [2:0] state = IDLE;                 // Neural Network State

reg [5:0] tot_node_cnt;                 // Counter keeps track of total nodes iterated in layer
reg [5:0] input_cnt;                    // Counter to iterate through inputs
reg conf_data_flag;                     // Flag used to determine when new config data in being read

reg [3:0] num_layers;                   // Stores number of nodes, max = 16
reg [3:0] current_layer;                // Stores the value of the current layer
reg [5:0] nodes_in_layer;               // # of nodes in current layer
reg [5:0] pre_layer_nodes;              // # of nodes in previous layer, used as input # value for current layer

reg [13:0] weight_mem_addr_ptr;         // Weight Memory Address
reg [9:0]  bias_mem_addr_ptr;           // Bias Memory Address
reg [4:0]  out_buf_mem_addr_ptr;        // Output Buffer Memory Address
// reg [2:0]  config_mem_addr_ptr;      // Configuration Memory Addr

// reg out_buf_busy;                       // Busy flag used to check if output buffer is loading new value

// Muxes for addresses of NNA Memories
assign weight_mem_addr = (nn_begin)? weight_mem_addr_ptr : addr[13:0];
assign bias_mem_addr = (nn_begin)? bias_mem_addr_ptr : addr[9:0];
assign out_buf_mem_addr = (nn_begin)? out_buf_mem_addr_ptr : addr[4:0];
assign config_mem_addr = (nn_begin)? current_layer[3:1] : addr[2:0];        // Uses current_layer/2 as address

assign num_layers = cmd[4:1];                   // Assign proper cmd bits to num_layers
assign op_code = cmd[15:13];                    // Assign proper cmd bits to op_code
assign nn_begin = cmd[0];                       // Assign proper cmd bit to nn_begin
assign out_ctrl = input_cnt[1:0];               // Assign output control to 2 most LSB of input_cnt


always @(posedge clk, rst) begin
    if (rst) begin
        tot_node_cnt <= 6'h00;
        current_layer <= 4'h0;
        input_cnt <= 6'h00;
        nodes_in_layer <= config_mem_data[5:0];
        pre_layer_nodes <= cmd[10:5];
        bias_ctrl <= 1'b0;
        conf_data_flag <= 1'b0;
        weight_mem_addr_ptr = 14'h0000; 
        bias_mem_addr_ptr = 10'h000;
        out_buf_mem_addr_ptr = 5'h0;
        next_op_code <= NOOP;
    end
    else if (!nn_begin) begin         // Neural Network has not been activated
        next_op_code <= op_code;
        out_buf_mem_wr_en <= 1'b0;
        
        if (op_code == OBMW) begin
            out_buf_mem_wr_en <= 1'b1;
        end
        // tot_node_cnt <= 6'h00;
        // current_layer <= 4'h0;
        // input_cnt <= 6'h00;
    end
    else if (nn_begin) begin          // Neural Network has been activated
        
        // next_op_code <= NN_START;
        // 
        // if (next_op_code == NN_START) begin       // Might need to remove this
        //     
        //     if (current_layer == num_layers) begin      // Final Layer has been reached
        //         
        //     end
        //     else if (tot_node_cnt >= nodes_in_layer) begin   // Every node in layer has been finished
        //         
        //     end
        //     else if (input_cnt == pre_layer_nodes) begin     // All inputs have been presented nodes
        //         
        //         tot_node_cnt <= tot_node_cnt + 4;
        //         input_cnt <= 6'h00;
        //         bias_ctrl <= 1'b1;
        //         bias_mem_addr_ptr <= bias_mem_addr_ptr + 1;
        //         
        //     end
        //     else begin
        //         bias_ctrl <= 1'b0;
        //         if (out_buf_busy == 1'b1) begin
        //             out_buf_busy <= 1'b0;
        //         end
        //         else if (out_buf_busy == 1'b0) begin
        //             input_cnt <= input_cnt + 1;
        //             
        //             if (input_cnt[1:0] == 3) begin
        //                 weight_mem_addr_ptr <= weight_mem_addr_ptr + 1;     // Increment Weight Address
        //                 out_buf_mem_addr_ptr <= out_buf_mem_addr_ptr + 1;   // Increment Output Buffer Address
        //                 out_buf_busy <= 1'b1;                               // Set Output Buffer Busy to read output buffer memory
        //                 out_buf_mem_wr_en <= 1'b0;
        //             end
        //         end
        //     end
        // end
    
    //states: IDLE, PRESENT_INPUT, UPDATE_INPUT, WRITE_OUTPUT, NEXT_LAYER, NN_FINISH
    
        unique case (state)
            IDLE: begin
                tot_node_cnt <= 6'h00;
                current_layer <= 4'h0;
                input_cnt <= 6'h00;
                nodes_in_layer <= config_mem_data[5:0];
                pre_layer_nodes <= cmd[10:5];
                bias_ctrl <= 1'b0;
                conf_data_flag <= 1'b0;
                weight_mem_addr_ptr = 14'h0000; 
                bias_mem_addr_ptr = 10'h000;
                out_buf_mem_addr_ptr = 5'h0;
                out_buf_mem_wr_en <= 1'b0;
                next_op_code <= NOOP;
                state <= PRESENT_INPUT;
            end
            PRESENT_INPUT: begin
                input_cnt <= input_cnt + 1;
                out_buf_mem_wr_en <= 1'b0;
                
                // CREATE ACTIVATION CONTROL SIGNAL AND KEEP IN MIND ROUTING
                
                if (input_cnt == pre_layer_nodes) begin
                    tot_node_cnt <= tot_node_cnt + 4;
                    input_cnt <= 6'h00;
                    bias_ctrl <= 1'b1;
                    bias_mem_addr_ptr <= bias_mem_addr_ptr + 1;
                    out_buf_mem_addr_ptr <= out_buf_mem_addr_ptr + 1;   // Increment Output Buffer Address
                    state <= WRITE_OUTPUT; // CHANGE THIS TO PRESENT_BIAS STATE
                                           // AFTER PRESENT_BIAS STATE GO INTO ACTIVATION_STATE
                                           // AFTER ACTIVATION_STATE GO INTO WRITE_OUTPUT STATE
                    
                end
                else if (input_cnt[1:0] == 3) begin
                    weight_mem_addr_ptr <= weight_mem_addr_ptr + 1;     // Increment Weight Address
                    out_buf_mem_addr_ptr <= out_buf_mem_addr_ptr + 1;   // Increment Output Buffer Address
                    out_buf_mem_wr_en <= 1'b0;
                    state <= UPDATE_INPUT;
                    // MIGHT NEED TO ACCOUNT FOR MAC AND WHAT IT SEES
                end
            end
            UPDATE_INPUT: begin
                state <= PRESENT_INPUT;
            end
            WRITE_OUTPUT: begin
                bias_ctrl <= 1'b0;
                out_buf_mem_wr_en <= 1'b1;
                
                // NEED TO THINK OF SOME WAY TO ALTERNATE ADDRESS SPACES FOR OUTPUT BUFFER
                
                if (tot_node_cnt >= nodes_in_layer) begin
                    pre_layer_nodes <= nodes_in_layer;
                    current_layer <= current_layer + 1;
                    if (!current_layer[0]) begin
                        nodes_in_layer <= config_mem_data[11:6];
                    end
                    else if (current_layer[0]) begin    // Addition to current_layer will move config_mem_addr by one
                        conf_data_flag <= 1'b1;
                    end
                    state <= NEXT_LAYER;
                end
                else begin
                    state <= PRESENT_INPUT;
                end
            end
            NEXT_LAYER: begin
                if (conf_data_flag) begin       // New Configuration Data needed to be read to obtain nodes_in_layer value
                    nodes_in_layer <= config_mem_data[5:0];
                    conf_data_flag <= 1'b0;
                end
                
                if (current_layer == num_layers) begin
                    state <= NN_FINISH;
                end
                else begin
                    input_cnt <= 6'h00;
                    tot_node_cnt <= 6'h00;
                    state <= PRESENT_INPUT;
                end
            end
            NN_FINISH: begin
                tot_node_cnt <= 6'h00;
                current_layer <= 4'h0;
                input_cnt <= 6'h00;
                bias_ctrl <= 1'b0;
                conf_data_flag <= 1'b0;
                weight_mem_addr_ptr = 14'h0000; 
                bias_mem_addr_ptr = 10'h000;
                out_buf_mem_addr_ptr = 5'h0;
                //state <= IDLE;
            end
        endcase
    end
end


always @(*) begin
    unique case (next_op_code)
        WMW:  begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b1;
            config_mem_wr_en    = 1'b0;
        end
        OBMW: begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b0;
        end
        CMW:  begin
            write_data_ctrl     = 1'b1;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b1;
        end
        BMW:  begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b1;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b0;
        end
        OBO: begin          // WILL REQUIRE MULTIPLE CLOCK CYCLES
            
        end
        NOOP: begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b0;
        end
        default: begin
            write_data_ctrl     = 1'b0;
            bias_mem_wr_en      = 1'b0;
            weight_mem_wr_en    = 1'b0;
            config_mem_wr_en    = 1'b0;
        end
    endcase
end

endmodule